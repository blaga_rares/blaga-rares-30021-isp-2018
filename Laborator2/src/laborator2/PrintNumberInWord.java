package laborator2;

import java.util.Scanner;

public class PrintNumberInWord {
    public static void main(String[] args) {
      Scanner x=new Scanner(System.in);
      System.out.println("Introduceti numarul:");
      int nr=x.nextInt();
   
      // Using nested-if
      if (nr == 1) {
         System.out.println("ONE");
      } else if (nr == 2) {
          System.out.println("TWO");
      } else if (nr == 3) {
          System.out.println("THREE");
      } else if (nr == 4) {
          System.out.println("FOUR");
      } else if (nr == 5) {
          System.out.println("FIVE");
      } else if (nr == 6) {
          System.out.println("SIX");
      } else if (nr == 7) {
          System.out.println("SEVEN");
      } else if (nr == 8) {
          System.out.println("EIGHT");
      } else if (nr == 9) {
          System.out.println("NINE");
      } else {
       
       System.out.println("OTHER");
       
      }

      // Using switch-case
        switch (nr) {
            case 1:
                System.out.println("ONE");
                break;
            case 2:
                System.out.println("TWO");
                break;
            case 3:
                System.out.println("THREE");
                break;
            case 4:
                System.out.println("FOUR");
                break;
            case 5:
                System.out.println("FIVE");
                break;
            case 6:
                System.out.println("SIX");
                break;
            case 7:
                System.out.println("SEVEN");
                break;
            case 8:
                System.out.println("EIGHT");
                break;
            case 9:
                System.out.println("NINE");
                break;
            default:
                System.out.println("OTHER");
                break;
        }
}
}
