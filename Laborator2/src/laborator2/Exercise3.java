package laborator2;

import java.util.Scanner;

public class Exercise3 {
      public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int A, B;
        boolean prim = true;
        int NrPrime = 0;

        System.out.print("introduceti primul numar ");
        A = scan.nextInt();
        System.out.print("introduceti ultimul numar ");
        B = scan.nextInt();

        System.out.print("numerele prime: ");
        for (int i = A; i <= B; i++)
        {
            prim = true;
            for (int j = 2; j <= i/2 ; j++)
            {
                if (i % j == 0)
                {
                    prim = false;
                }
            }
            if (prim == true && i != 1)
            {
                System.out.print(i + " ");
                NrPrime++;
            }
        }
        if (NrPrime == 0)
        {
            System.out.print("nu exista numere prime");
        }
        else
        {
            System.out.println(" sunt " + NrPrime + " numere prime");
        }
    }
}