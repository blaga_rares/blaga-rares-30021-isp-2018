package laborator2;

import java.util.Random;

public class Exercise5 {

public static void main(String[] args) {
    int v[]=new int[10];
    Random s = new Random();
    
    for (int i=0;i<10;i++)
    {
        v[i]= 1 + s.nextInt(100);
    }
    System.out.println("inainte de sortare:");
    for(int i=0;i<10;i++)
    {
        System.out.println("v[" + i + "] = " + v[i]);
    }
    
    boolean sort=false;
    while(!sort) {
        sort=true;
        for(int i=0;i<9;i++){
            if(v[i] > v[i+1]) {
                int t=v[i];
                v[i] = v[i+1];
                v[i+1] = t;
                sort=false;
            }
    }
    }
System.out.println();
System.out.println("Dupa sortare:");
for(int i= 0;i<10;i++){
    System.out.println("v[" + i + "]=" + v[i]);
}
}
}