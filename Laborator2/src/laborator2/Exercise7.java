package laborator2;

import java.util.Scanner;
import java.util.Random;

public class Exercise7 {
     public static void main(String[] args) {
            Random aleatoriu = new Random();
            Scanner s = new Scanner(System.in);
            int incercarenum = 0;
            int maxincercari = 3;
            int n = aleatoriu.nextInt(99)+1;
            int numar;
        do {
                System.out.print("Gues the number: ");
                numar = s.nextInt();

                if (numar > n)
                    System.out.println("Wrong answer , your number is too high " );
                else if (numar < n)
                    System.out.println("Wrong answer, your number is too low " );
                else
                    System.out.println("Corect! " + numar );
            } while (numar != n && ++incercarenum<maxincercari);
            if (incercarenum==maxincercari)
                System.out.println("You lost, the number was "+n);
        }
    }
