
package laborator4ex3;

public class Professor extends Person {
    int NrProfesor;


    public Professor(int nr,String Nume,int varsta){
        super(varsta,Nume);
        NrProfesor = nr;
    }



    public int getNrProfesor() {
        return NrProfesor;
    }

    public String toString() {
        return "Persoana este un profesor si numele lui este :" + getNume() + " ,varsta lui este:" + getVarsta() + " si numarul lui este " + getNrProfesor();
    }
}
