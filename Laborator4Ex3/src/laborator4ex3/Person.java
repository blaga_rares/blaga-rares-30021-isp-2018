
package laborator4ex3;

public class Person {
    String Nume;
    int varsta;
    public Person(int varsta, String Nume){
        this.varsta=varsta;
        this.Nume=Nume;
    }

    public Person() {
    Nume="Mark";
    varsta=20;
    }

    int getVarsta(){
        return this.varsta;
    }
    String getNume(){
        return this.Nume;
    }
    public String toString(){
        return "Numele persoanei este:"+this.Nume+" si varsta lui este:"+getVarsta();
    }

    public static void main(String[] args) {
        Person Mark=new Person();
        System.out.println(Mark.toString());
        Person n = new Professor(11,"Rey",35);
        System.out.println(n.toString());
        Person m=new Student(7,"Stannis",25);
        System.out.println(m.toString());

    }
}
