package laborator4ex3;

public class Student extends Person {
    int NrStudent;
    public Student(int nr, String Nume,int varsta){
        super(varsta,Nume);
        NrStudent=nr;
    }
    int getNrStudent(){
        return NrStudent;
    }
    public String toString(){
        return "Persoana este un student si numele lui este:"+getNume()+" ,varsta lui este:"+getVarsta()+" si numarul lui este "+getNrStudent();
    }
}