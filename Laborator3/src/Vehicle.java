public class Vehicle {
    String model;
    int viteza;
    String TipCombustibil;

    Vehicle(){
        model="Dacia";
        viteza=50;
        TipCombustibil="Benzina";
    }
    Vehicle(String m,int v,String c)
    {
        model=m;
        viteza=v;
        TipCombustibil=c;
    }
    public String getModel(){
        return model;

    }
    public String getTipCombustibil(){
        return TipCombustibil;
    }
    public int getViteza(){
        return viteza;
    }

    public void accelerare (){
        viteza+=10;
    }
    public void franare(){
        viteza-=3;
    }
    public void stop(){
        viteza=0;
    }
    public String toString(){
        return "Modelul masini este:"+model+" viteza ei este = "+viteza+" si tipul combustibilul este "+TipCombustibil;
    }

    public static void main(String[] args) {
        Vehicle v1=new Vehicle();
        System.out.println("Modelul masini este: "+v1.getModel()+" viteza este:"+v1.getViteza()+" si tipul combustibilul este: "+v1.getTipCombustibil());
        Vehicle v2=new Vehicle("BMW",90,"Diesel");
        System.out.println(v2);
        for(int i=0;i<5;i++){
            v2.accelerare();
            if (i==2)
                v2.franare();
        }
        System.out.println(v2);
        Vehicle v3=new Vehicle("Ferrari",140,"Benzina");
        System.out.println(v3);
        do{
            v3.franare();
        }while(v3.viteza!=0);
        System.out.println(v3);
    }

}
//ex1